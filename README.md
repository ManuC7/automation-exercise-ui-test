# Automation Exercise UI Test Suite

This repository contains a Cypress test suite for verifying the functionality of the `https://automationexercise.com/` website. The tests focus on validating the complete user flow from selecting an item, register, and checkout order.

## Prerequisites

- Node.js and npm installed on your machine.

## Setup

1. **Clone this repository:**

   ```bash
   git clone https://github.com/ManuC7/automation-exercise-ui-test.git
   ```

2. **Navigate to the project directory:**

   ```bash
   cd automation-exercise-ui-test
   ```

3. **Install dependencies:**

   ```bash
   npm install
   ```

## Configuration

1. **Create a `cypress.env.json` file in the project root with the following content:**

   ```bash
   {
     "CYPRESS_USERNAME": "username",
     "CYPRESS_EMAIL": "some@email.com",
     "CYPRESS_PASSWORD": "password"
   }
   ```

   Replace `"username"`, `"password"`, and `"some@email.com"` with your custom credentials.

## Running Tests

To run the tests in headless mode and generate a Mochawesome report, use the following command:

```bash
npm run test-headless
```

This command runs Cypress in headless mode, using Chrome as the browser, and generates a Mochawesome report.  
The test reports can be found in the reports folder.

To run the tests with the Cypress Test Runner and view the reports interactively, use:

```bash
npm run test
```

This command runs Cypress in interactive mode, and opens the Cypress Test Runner.

# Test Plan

## Scope

End-to-end regression test suite covering major user flows:

- Product browsing
- Adding items to cart
- User registration and login
- Checkout and payments
- Order confirmation
- Contact form submission

## Tools & Frameworks

- Cypress as the test framework
- Page Object Model for element selectors and actions
- Fixtures for test data
- GitLab for source control

## Test Cases

- Add products to cart, register, and pay order.
- Log in an existing user and submit the contact form.

## Test Strategy

- Modular page objects for abstraction
- Parameterized test data
- Credential management for secure login
- UI interactions with edge cases
- End-to-end workflow validation
- Simulate real user journeys
- Cross-browser testing
- Retry flaky tests

## Reporting

- Mochawesome for detailed HTML test reports
- Screenshots/videos for test artifacts
- CI integration to track test statuses

This overall strategy focuses on automating real-world scenarios following key best practices using Cypress and proven patterns like page objects to develop a maintainable and reusable test suite.

## Test Descriptions

1. **Test Case 1: Add products to cart, register, and pay order.**

#### Test Case Identification: E2E-001

#### Description:

Verify the end-to-end process of adding products to the cart, registering a new user, and completing the payment.

#### Steps Taken:

- Scroll down to the middle of the page.
- Select a product from the middle.
- Set the product quantity to 30.
- Add the product to the cart.
- Proceed to checkout from the cart.
- Go to register from the modal.
- Signup with valid information.
- Fill the login form.
- Submit the login form.
- Validate successful account creation.
- Go to the cart.
- Proceed to checkout.
- Add a comment in the checkout.
- Place the order.
- Fill the payment form.
- Submit the payment form.
- Logout.

#### Expected Results:

- The user successfully goes through each step without encountering errors.

#### Actual Results:

- All steps executed without errors.

#### Pass/Fail Status:

- Pass

2. **Test Case 2: Log in an existing user and submit the contact form.**

   #### Test Case Identification: E2E-002

#### Description:

Validate the process of logging in with an existing user and submitting a contact form.

#### Steps Taken:

- Go to the login page.
- Login with valid credentials.
- Go to the "Contact Us" page.
- Fill in the contact form.
- Submit the contact form.
- Logout.

#### Expected Results:

- The user can successfully log in, submit a contact form, and logout.

#### Actual Results:

- All steps executed without errors.

#### Pass/Fail Status:

- Pass

## Dependencies

- Cypress: ^13.6.1
- Cypress Mochawesome Reporter: ^3.7.0

## Test Location

The test script is located at cypress/e2e/automationExerciseUI.cy.js.

## Reports

The test reports can be found in the reports folder.
