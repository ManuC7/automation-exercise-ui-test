class AccountCreated {
  elements = {
    title: '[data-qa="account-created"]',
    continueBtn: '[data-qa="continue-button"]',
  };

  validateAccountCreation() {
    cy.get(this.elements.title).should("have.text", "Account Created!");
    cy.get(this.elements.continueBtn).contains("Continue").click();
    return this;
  }
}

export default AccountCreated;
