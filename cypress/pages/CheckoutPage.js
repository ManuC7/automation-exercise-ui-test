class CheckoutPage {
  elements = {
    commentTextArea: "textarea",
    placeOrderBtn: ".btn.btn-default.check_out",
  };
  addComment() {
    cy.get(this.elements.commentTextArea).type(
      "A comment about the product..."
    );
  }

  placeOrder() {
    cy.get(this.elements.placeOrderBtn)
      .should("have.text", "Place Order")
      .click();
  }
}

export default CheckoutPage;
