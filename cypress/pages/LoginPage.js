class LoginPage {
  elements = {
    signupInputName: '[data-qa="signup-name"]',
    signupInputEmail: '[data-qa="signup-email"]',
    signupBtn: '[data-qa="signup-button"]',
    loginInputEmail: '[data-qa="login-email"]',
    loginInputPassword: '[data-qa="login-password"]',
    loginBtn: '[data-qa="login-button"]',
    loginForm: ".login-form",
    genderCheck: "#id_gender1",
    formInputPassword: '[data-qa="password"]',
    formSelectDays: '[data-qa="days"]',
    formSelectMonths: '[data-qa="months"]',
    formSelectYears: '[data-qa="years"]',
    formInputFirstName: '[data-qa="first_name"]',
    formInputLastName: '[data-qa="last_name"]',
    formInputCompany: '[data-qa="company"]',
    formInputAddress: '[data-qa="address"]',
    formInputAddress2: '[data-qa="address2"]',
    formSelectCountry: '[data-qa="country"]',
    formInputState: '[data-qa="state"]',
    formInputCity: '[data-qa="city"]',
    formInputZipcode: '[data-qa="zipcode"]',
    formInputMobileNumber: '[data-qa="mobile_number"]',
    formCreateAccountBtn: '[data-qa="create-account"]',
  };

  goToLogin() {
    cy.visit("/login");
  }

  signup() {
    cy.get(this.elements.signupInputName)
      .clear()
      .type(Cypress.env("CYPRESS_USERNAME"));
    cy.get(this.elements.signupInputEmail)
      .clear()
      .type(Cypress.env("CYPRESS_EMAIL"));
    cy.get(this.elements.signupBtn).click();
    cy.get(this.elements.loginForm).should(
      "contain",
      "Enter Account Information"
    );
    return this;
  }

  fillLoginForm() {
    cy.get(this.elements.genderCheck).check();
    cy.get(this.elements.formInputPassword).type(
      Cypress.env("CYPRESS_PASSWORD")
    );
    cy.get(this.elements.formSelectDays).select("10");
    cy.get(this.elements.formSelectMonths).select("October");
    cy.get(this.elements.formSelectYears).select("2000");
    cy.fixture("user").then((user) => {
      cy.get(this.elements.formInputFirstName).type(user.firstName);
      cy.get(this.elements.formInputLastName).type(user.lastName);
      cy.get(this.elements.formInputCompany).type(user.company);
      cy.get(this.elements.formInputAddress).type(user.address);
      cy.get(this.elements.formInputAddress2).type(user.address2);
      cy.get(this.elements.formSelectCountry).select(user.country);
      cy.get(this.elements.formInputState).type(user.state);
      cy.get(this.elements.formInputCity).type(user.city);
      cy.get(this.elements.formInputZipcode).type(user.zipcode);
      cy.get(this.elements.formInputMobileNumber).type(user.mobileNumber);
    });
    return this;
  }

  submitForm() {
    cy.get(this.elements.formCreateAccountBtn).click();
    return this;
  }

  login() {
    cy.get(this.elements.loginInputEmail)
      .clear()
      .type(Cypress.env("CYPRESS_EMAIL"));
    cy.get(this.elements.loginInputPassword)
      .clear()
      .type(Cypress.env("CYPRESS_PASSWORD"));
    cy.get(this.elements.loginBtn).click();
  }
}

export default LoginPage;
