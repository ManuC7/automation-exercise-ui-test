class PaymentPage {
  elements = {
    nameOnCardInput: '[data-qa="name-on-card"]',
    cardNumberInput: '[data-qa="pay-button"]',
    cvcInput: '[data-qa="cvc"]',
    expirationMonthInput: '[data-qa="expiry-month"]',
    expirationYearInput: '[data-qa="expiry-year"]',
    payAndConfirmBtn: '[data-qa="pay-button"]',
    orderPlacedTitle: '[data-qa="order-placed"]',
    continueBtn: '[data-qa="continue-button"]',
  };

  fillPaymentForm() {
    cy.fixture("creditCard").then((creditCardInfo) => {
      cy.get(this.elements.nameOnCardInput).clear().type(creditCardInfo.name);
      cy.get(this.elements.cardNumberInput).type(creditCardInfo.number);
      cy.get(this.elements.cvcInput).type(creditCardInfo.cvc);
      cy.get(this.elements.expirationMonthInput).type(
        creditCardInfo.expirationMonth
      );
      cy.get(this.elements.expirationYearInput).type(
        creditCardInfo.expirationYear
      );
    });
  }

  submitPaymentForm() {
    cy.get(this.elements.payAndConfirmBtn)
      .should("have.text", "Pay and Confirm Order")
      .click();
    cy.get(this.elements.orderPlacedTitle).should("have.text", "Order Placed!");
    cy.get(this.elements.continueBtn).click();
  }
}
export default PaymentPage;
