class ProductDetailsPage {
  elements = {
    inputQuantity: "#quantity",
    addToCartBtn: ".cart",
    confirmModalTitle: ".modal-title",
    viewCartLink: 'a[href="/view_cart"]',
    checkoutBtn: ".check_out",
  };

  setProductQuantity() {
    cy.get(this.elements.inputQuantity).clear().type("30");
    return this;
  }

  addToCartProducts() {
    cy.get(this.elements.addToCartBtn).click();
    cy.get(this.elements.confirmModalTitle).should("have.text", "Added!");
    return this;
  }
  goToCartFromModal() {
    cy.get(this.elements.viewCartLink).contains("View Cart").click();
    return this;
  }
  proceedToCheckout() {
    cy.get(this.elements.checkoutBtn)
      .should("have.text", "Proceed To Checkout")
      .click();
    return this;
  }
}

export default ProductDetailsPage;
