class HomePage {
  elements = {
    featuresItems: ".features_items >>> .choose",
    viewProductLink: 'a[href*="product_details"]',
    loginLink: 'a[href="/login"]',
    logoutLink: 'a[href="/logout"]',
    deleteAccountLink: 'a[href="/delete_account"]',
    viewCartLink: 'li > a[href="/view_cart"]',
    contactUsLink: 'a[href="/contact_us"]',
  };

  enterUrl() {
    cy.visit("/");
    return this;
  }

  getMiddleIndex() {
    cy.get(this.elements.featuresItems).then(($items) => {
      const total = Math.floor($items.length / 2);
      cy.wrap(total).as("middleIndex");
    });
  }

  scrollToMiddle() {
    this.getMiddleIndex();
    cy.get("@middleIndex").then((middleIndex) => {
      cy.get(this.elements.featuresItems).eq(middleIndex).scrollIntoView();
    });
    return this;
  }

  selectMiddleProduct() {
    this.getMiddleIndex();
    cy.get("@middleIndex").then((middleIndex) => {
      cy.get(this.elements.viewProductLink).eq(middleIndex).click();
    });
    return this;
  }

  deleteAccount() {
    cy.get(this.elements.deleteAccountLink).click();
    cy.url().should("contain", "delete_account");
  }

  goToCart() {
    cy.get(this.elements.viewCartLink).click();
  }

  logout() {
    cy.get(this.elements.logoutLink).click();
  }

  goToContactUs() {
    cy.get(this.elements.contactUsLink).click();
  }
}

export default HomePage;
