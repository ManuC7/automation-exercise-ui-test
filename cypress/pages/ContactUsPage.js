class ContactUsPage {
  elements = {
    nameInput: '[data-qa="name"]',
    emailInput: '[data-qa="email"]',
    subjectInput: '[data-qa="subject"]',
    messageTextarea: '[data-qa="message"]',
    uploadFileInput: '[name="upload_file"]',
    submitFormBtn: '[data-qa="submit-button"]',
  };

  fillContactUsForm() {
    cy.fixture("user").then((user) => {
      cy.get(this.elements.nameInput).type(user.firstName);
      cy.get(this.elements.emailInput).type(user.email);
      cy.get(this.elements.subjectInput).type(user.subject);
      cy.get(this.elements.messageTextarea).type(user.message);
      cy.get(this.elements.uploadFileInput).selectFile("sample.txt");
    });
    return this;
  }

  submitContactUsForm() {
    cy.get(this.elements.submitFormBtn).click();
  }
}
export default ContactUsPage;
