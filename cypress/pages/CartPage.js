class CartPage {
  elements = {
    checkoutBtn: ".check_out",
    registerLoginLink: 'a[href="/login"]',
  };

  goToCart() {
    cy.visit("/view_cart");
  }

  proceedToCheckout() {
    cy.get(this.elements.checkoutBtn)
      .should("have.text", "Proceed To Checkout")
      .click();
    return this;
  }

  goToRegisterFromModal() {
    cy.get(this.elements.registerLoginLink).contains("Register").click();
    return this;
  }
}

export default CartPage;
