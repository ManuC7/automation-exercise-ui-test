import AccountCreated from "../pages/AccountCreated";
import CheckoutPage from "../pages/CheckoutPage";
import HomePage from "../pages/HomePage";
import PaymentPage from "../pages/PaymentPage";
import CartPage from "../pages/cartPage";
import ContactUsPage from "../pages/contactUsPage";
import LoginPage from "../pages/loginPage";
import ProductDetailsPage from "../pages/productDetailsPage";

const homePage = new HomePage();
const productDetailsPage = new ProductDetailsPage();
const cartPage = new CartPage();
const loginPage = new LoginPage();
const accountCreated = new AccountCreated();
const checkoutPage = new CheckoutPage();
const paymentPage = new PaymentPage();
const contactUsPage = new ContactUsPage();

describe("Clothing Store E2E Tests.", () => {
  beforeEach(() => {
    homePage.enterUrl();
  });

  it("add products to cart, register and pay order.", () => {
    homePage.scrollToMiddle();
    homePage.selectMiddleProduct();
    productDetailsPage.setProductQuantity();
    productDetailsPage.addToCartProducts();
    productDetailsPage.goToCartFromModal();
    cartPage.proceedToCheckout();
    cartPage.goToRegisterFromModal();
    loginPage.signup();
    loginPage.fillLoginForm();
    loginPage.submitForm();
    accountCreated.validateAccountCreation();
    homePage.goToCart();
    cartPage.proceedToCheckout();
    checkoutPage.addComment();
    checkoutPage.placeOrder();
    paymentPage.fillPaymentForm();
    paymentPage.submitPaymentForm();
    homePage.logout();
  });

  it("logs in existing user and submits contact form.", () => {
    loginPage.goToLogin();
    loginPage.login();
    homePage.goToContactUs();
    contactUsPage.fillContactUsForm();
    contactUsPage.submitContactUsForm();
    homePage.logout();
  });

  after(() => {
    loginPage.goToLogin();
    loginPage.login();
    homePage.deleteAccount();
  });
});
