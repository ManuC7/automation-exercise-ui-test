import HomePage from "../pages/HomePage";
import LoginPage from "../pages/loginPage";

Cypress.Commands.add("deleteAccount", () => {
  const loginPage = new LoginPage();
  const homePage = new HomePage();

  cy.visit("/login");
  loginPage.login();
  homePage.deleteAccount();
});
